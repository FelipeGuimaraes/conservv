<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ page import="models.Realiza" %>
<%@ page import="models.PessoaJuridica" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.swing.text.MaskFormatter" %>

<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="#">



    <title>conserv</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=latin-ext" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/range.js"></script>

    <link href="css/style.css" rel="stylesheet">


    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <link href="css/switches.css" rel="stylesheet">
    <link href="css/selectBox.css" rel="stylesheet">
    <link href="css/modal.css" rel="stylesheet">
    <script type="text/javascript" src="js/modal.js"></script>
    <script type="text/javascript" src="js/servicos.js"></script>
    <script type="text/javascript" src="js/selectBox.js"></script>
    <link href="css/rangeSlider.css" rel="stylesheet">
    <link href="css/rangeSliderskin.css" rel="stylesheet">
    <script type="text/javascript" src="js/rangeSlider.js"></script>
    <script type="text/javascript" src="js/changeImage.js"></script>
    <script type="text/javascript" src="js/buscaServAjax.js"></script>
    <script type="text/javascript" src="js/filtros.js"></script>
    <script type="text/javascript" src="js/paginacaoServ.js"></script>

</head>

<body>
    <%
		// Recupera os resultados da busca vindos do servlet
		ArrayList<Realiza> servicos = (ArrayList<Realiza>) request.getAttribute("servicos");
	%>

    <div class="site-wrapper">

        <div class="site-wrapper-inner">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand"><a href="index.html"><img class="logo" src="imagens/logo.png" alt=""></a>
                    </h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li class="active"><a href="index.html">home</a></li>
                            <li><a href="cidade.html">na sua cidade</a></li>
                            <li><a href="sobre.html">sobre</a></li>
                            <li><a href="contato.html">contato</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="container">

                <div class="inner">

                    <h1 class="heading">O que você procura?</h1>
                    <p class="lead">Digite aqui o serviço e a faixa de preço desejados</p>

                    <form id="frmIni">
                        <div class="form-row">
                            <div class="form-group col-sm-3">
                                <input type="text" id="iniServ" class="form-control" <% if (servicos.size() > 0) {%>
                                    value="<%= servicos.get(0).getServico().getNomeServico() %>" <%} %> />
                            </div>

                            <div class="form-group col-sm-2">
                                <input type="text" id="range_03">
                            </div>

                            <div class="form-group col-sm-2 col-cb">
                                <label class="switch">
                                    <input type="checkbox" id="cbPresencial" value="presencial"
                                        onchange="filtroPresencial()">
                                    <span class="slider round"></span>
                                    <label id="lbPresencial" for="cbPresencial">Presencial</label>
                                </label>
                            </div>

                            <div class="form-group col-sm-2">
                                <label class="switch">
                                    <input type="checkbox" id="cbSeguro" value="seguro" onchange="filtroSeguro()">
                                    <span class="slider round"></span>
                                    <label id="lbSeguro" for="cbSeguro">Seguro</label>
                                </label>
                            </div>

                            <div class="form-group col-sm-2 slBox">
                                <div class="select_mate" data-mate-select="active">
                                    <select onclick="return false;" id="slOrdem">
                                        <option value="" disabled selected>Ordenar por... </option>
                                        <option value="1">R$: menor para maior</option>
                                        <option value="2">R$: maior para menor</option>
                                    </select>
                                    <p class="selecionado_opcion" onclick="open_select(this)"></p><span
                                        onclick="open_select(this)" class="icon_select_mate"><svg fill="#000000"
                                            height="24" viewBox="0 0 24 24" width="24"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
                                            <path d="M0-.75h24v24H0z" fill="none" />
                                        </svg></span>
                                    <div class="cont_list_select_mate" style="background: green">
                                        <ul class="cont_select_int"> </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="row-btn">
                                <button type="submit" class="btn btn-primary" id="procuraServ">Procurar</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div id="modal_dados" class="modal">
                    <div id="print">

                        <h4 id="modal_pj"></h4>
                        <p id="modal_cnpj"><strong>CNPJ: </strong></p>
                        <p id="modal_end"><strong>Endereço: </strong></p>
                        <p id="modal_cep"><strong>CEP: </strong></p>
                        <p id="modal_tel"><strong>Telefone: </strong></p>
                        <p id="modal_email"><strong>E-mail: </strong></p>
                        <hr />
                        <p><strong id="modal_seg_pres"></strong></p>
                        <p id="modal_duracao"><strong>Duração: </strong> meses</p>
                        <p id="modal_valor"><strong>Valor: </strong> R$</p>

                    </div>

                    <button class="btn btn-primary" id="imprimir" onclick="printModal()">Imprimir</button>
                </div>

                <div class="datagrid">

                    <table class="table table-striped" id="datatable">
                        <%
	                            if (servicos.size() > 0) {
	                            	
	                            	// Inicializa as masks
	                            	MaskFormatter mask_cnpj = new MaskFormatter("##.###.###/####-##");
			                        MaskFormatter mask_tel = new MaskFormatter("(##) ####-####");
			                        MaskFormatter mask_cep = new MaskFormatter("##.###-###");
			
			                        mask_cnpj.setValueContainsLiteralCharacters(false);
			                        mask_tel.setValueContainsLiteralCharacters(false);
			                        mask_cep.setValueContainsLiteralCharacters(false);
	                        %>
                        <thead>
                            <tr>
                                <th>Serviço</th>
                                <th>Pessoa Jurídica</th>
                                <th>Duração</th>
                                <th>Valor</th>
                                <th>Ver mais</th>
                            </tr>
                        </thead>

                        <tbody id="resultados">
                            <%
					                        int id = 0; // id dos modals
			                                
			                                for (int i = 0; i < 10; i++) { // primeira pagina
			                                	
			                        %>

                            <tr>
                                <td><%= servicos.get(i).getServico().getNomeServico() %></td>
                                <td id="item_pj"><%= servicos.get(i).getPessoaJuridica().getNome() %></td>
                                <td id="item_duracao"><%= servicos.get(i).getDuracao() %> meses</td>
                                <td id="item_valor">R$ <%= servicos.get(i).getValor() %></td>
                                <td>
                                    <a class="btn btn-primary" href="#modal_dados" rel="modal:open"
                                        onclick="atualizarModalServ(<%= id %>)">+</a>
                                </td>
                                <td id="item_cnpj" class="invisible">
                                    <%= mask_cnpj.valueToString(servicos.get(i).getPessoaJuridica().getCnpj()) %></td>
                                <td id="item_end" class="invisible">
                                    <%= servicos.get(i).getPessoaJuridica().getEndereco() %></td>
                                <td id="item_cep" class="invisible">
                                    <%= mask_cep.valueToString(servicos.get(i).getPessoaJuridica().getCep()) %></td>
                                <td id="item_tel" class="invisible">
                                    <%= mask_tel.valueToString(servicos.get(i).getPessoaJuridica().getTelefone()) %>
                                </td>
                                <td id="item_email" class="invisible">
                                    <%= servicos.get(i).getPessoaJuridica().getEmail() %></td>
                                <td id="item_seguro" class="invisible"> <%= servicos.get(i).isSeguro() %></td>
                                <td id="item_pres" class="invisible"> <%= servicos.get(i).getServico().getTipo() %></td>
                                <td id="item_cidade" class="invisible">
                                    <%= servicos.get(i).getPessoaJuridica().getCidade() %></td>
                                <td id="item_estado" class="invisible">
                                    <%= servicos.get(i).getPessoaJuridica().getEstado() %></td>
                            </tr>

                            <%
		                        				id++;
		                                	}
                            	}
                        
                            	else {
                            %>
                            <tr id="nenhumRegistro">
                                Nenhum registro encontrado!
                            </tr>
                            <%
                            	}
                            %>

                        </tbody>
                    </table>

                    <nav class="mx-auto" style="width: 200px;">
                        <ul class="pagination">
                            <li class="page-item" id="anterior"><a class="page-link"
                                    onclick="mudarPagina('anterior')">Anterior</a></li>
                            <li class="page-item" id="pagina_1"><a class="page-link" onclick="mudarPagina(0)">1</a></li>
                            <li class="page-item" id="pagina_2"><a class="page-link" onclick="mudarPagina(1)">2</a></li>
                            <li class="page-item" id="pagina_3"><a class="page-link" onclick="mudarPagina(2)">3</a></li>
                            <li class="page-item" id="pagina_4"><a class="page-link" onclick="mudarPagina(3)">4</a></li>
                            <li class="page-item" id="pagina_5"><a class="page-link" onclick="mudarPagina(4)">5</a></li>
                            <li class="page-item" id="proxima"><a class="page-link"
                                    onclick="mudarPagina('proxima')">Proxima</a></li>
                        </ul>
                    </nav>

                </div>


            </div>

        </div>

    </div>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</body>

</html>