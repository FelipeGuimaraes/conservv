<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="models.Realiza" %>
<%@ page import="models.PessoaJuridica" %>
<%@ page import="models.Servico" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javax.swing.text.MaskFormatter" %>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="#">

    <title>conserv</title>

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">


    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=latin-ext" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script type="text/javascript" src="js/range.js"></script>
    <link href="css/style.css" rel="stylesheet">


    <link href="css/mapa.css" rel="stylesheet">



    <meta name=viewport content="width=device-width, initial-scale=1">


    <link href="css/switches.css" rel="stylesheet">
    <link href="css/selectBox.css" rel="stylesheet">
    <link href="css/modal.css" rel="stylesheet">
    <script type="text/javascript" src="js/modal.js"></script>
    <script type="text/javascript" src="js/selectBox.js"></script>
    <link href="css/rangeSlider.css" rel="stylesheet">
    <link href="css/rangeSliderskin.css" rel="stylesheet">
    <script type="text/javascript" src="js/rangeSlider.js"></script>
    <script type="text/javascript" src="js/changeImage.js"></script>
    <script type="text/javascript" src="js/buscaCidadeAjax.js"></script>
    <script type="text/javascript" src="js/paginacaoCidade.js"></script>

</head>

<body>
    <%
		// Recupera os resultados da busca vindos do servlet
		ArrayList<PessoaJuridica> pJuridicas = (ArrayList<PessoaJuridica>) request.getAttribute("pessoas_juridicas");
	%>

    <div class="site-wrapper">

        <div class="site-wrapper-inner">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand"><a href="index.html"><img class="logo" src="imagens/logo.png" alt=""></a>
                    </h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li><a href="index.html">home</a></li>
                            <li class="active"><a href="cidade.html">na sua cidade</a></li>
                            <li><a href="sobre.html">sobre</a></li>
                            <li><a href="contato.html">contato</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="container">

                    <div class="inner">

                        <h1 class="heading">Qual a sua cidade?</h1>
                        <p class="lead">Digite aqui a sua cidade e nós vamos encontrar PJs para você</p>

                        <form id="frmIni">
                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <input type="text" id="iniServ" class="form-control"
                                        <% if (pJuridicas.size() > 0) {%> value="<%= pJuridicas.get(0).getCidade() %>"
                                        <%} %> />
                                </div>

                                <div class="form-group col-sm-2">
                                    <button type="submit" class="btn btn-primary" id="procuraCid">Procurar</button>
                                </div>

                            </div>
                        </form>
                    </div>


                    <div id="dados" class="modal">
                        <div id="print">
                            <h4 id="modal_pj"></h4>
                            <p id="modal_cnpj"><strong>CNPJ:</strong></p>
                            <p id="modal_end"></p>
                            <p id="modal_cep"><strong>CEP:</strong></p>
                            <p id="modal_tel"><strong>Telefone:</strong></p>
                            <p id="modal_email"><strong>E-mail:</strong></p>
                            <hr />
                            <div id="modal_serv"><strong>Serviços disponíveis:</strong></div>

                            <div id="map"></div>
                            <script async defer
                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEqhUvNWYm5zlkH9HC7N2BDbQxzoEhliE&callback=initMap">
                                </script>

                        </div>
                        <br />
                        <button class="btn btn-primary" id="imprimir" onclick="printModal()">Imprimir</button>

                    </div>




                    <div class="datagrid">

                        <table class="table table-striped" id="datatable">
                            <%
	                            if (pJuridicas.size() > 0) {
	                            	
	                            	// Inicializa as masks
	                            	MaskFormatter mask_cnpj = new MaskFormatter("##.###.###/####-##");
			                        MaskFormatter mask_tel = new MaskFormatter("(##) ####-####");
			                        MaskFormatter mask_cep = new MaskFormatter("##.###-###");
			
			                        mask_cnpj.setValueContainsLiteralCharacters(false);
			                        mask_tel.setValueContainsLiteralCharacters(false);
			                        mask_cep.setValueContainsLiteralCharacters(false);
	                        %>

                            <thead>
                                <tr>
                                    <th>Pessoa Jurídica</th>
                                    <th>Cidade</th>
                                    <th>Telefone</th>
                                    <th>Ver mais</th>
                                </tr>
                            </thead>

                            <tbody id="resultados">
                                <%
					                        int id = 0; // id dos modals
			                                
			                                for (int i = 0; i < 10; i++) {
			                        %>
                                <tr>
                                    <td id="item_pj"><%= pJuridicas.get(i).getNome() %></td>
                                    <td id="item_cidade"><%= pJuridicas.get(i).getCidade() %></td>
                                    <td id="item_tel"><%= mask_tel.valueToString(pJuridicas.get(i).getTelefone()) %>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary open" href="#dados" rel="modal:open" id="openModal"
                                            onclick="atualizarModalCidade(<%= id %>)">+</a>
                                    </td>
                                    <td id="item_cnpj" class="invisible">
                                        <%= mask_cnpj.valueToString(pJuridicas.get(i).getCnpj()) %>
                                    </td>
                                    <td id="item_end" class="invisible"><%= pJuridicas.get(i).getEndereco() %></td>
                                    <td id="item_estado" class="invisible"><%= pJuridicas.get(i).getEstado() %></td>
                                    <td id="item_cep" class="invisible">
                                        <%= mask_cep.valueToString(pJuridicas.get(i).getCep()) %></td>
                                    <td id="item_email" class="invisible"><%= pJuridicas.get(i).getEmail() %></td>
                                    <td id="item_serv" class="invisible">
                                        <%
			                                    		for (Servico serv: pJuridicas.get(i).getServicoList()) {
			                                    	%>
                                        <span><%= serv.getNomeServico() %></span>
                                        <%
			                                    		}
			                                    	%>
                                    </td>
                                </tr>
                                <%
			                    				id++;
			                                }
	                            } else {
			                %>
                                <tr id="nenhumRegistro">
                                    Nenhum registro encontrado!
                                </tr>
                                <%
	                            }
		                    %>

                            </tbody>
                        </table>

                        <nav class="mx-auto" style="width: 200px;">
                            <ul class="pagination">
                                <li class="page-item" id="anterior"><a class="page-link"
                                        onclick="mudarPagina('anterior')">Anterior</a></li>
                                <li class="page-item" id="pagina_1"><a class="page-link" onclick="mudarPagina(0)">1</a>
                                </li>
                                <li class="page-item" id="pagina_2"><a class="page-link" onclick="mudarPagina(1)">2</a>
                                </li>
                                <li class="page-item" id="pagina_3"><a class="page-link" onclick="mudarPagina(2)">3</a>
                                </li>
                                <li class="page-item" id="pagina_4"><a class="page-link" onclick="mudarPagina(3)">4</a>
                                </li>
                                <li class="page-item" id="pagina_5"><a class="page-link" onclick="mudarPagina(4)">5</a>
                                </li>
                                <li class="page-item" id="proxima"><a class="page-link"
                                        onclick="mudarPagina('proxima')">Proxima</a></li>
                            </ul>
                        </nav>

                    </div>

                </div>
            </div>

        </div>

    </div>

    <!--<script src="https://openlayers.org/en/v4.6.5/build/ol.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="js/ol.js" type="text/javascript"></script>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</body>

</html>