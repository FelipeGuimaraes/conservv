var pagina = 0;

function mudarPagina(pg) {
	if (pg == 'anterior') {
		if (pagina > 0) {
			pagina -= 1;
		}
	} else if (pg == 'proxima') {
		pagina += 1;
	} else {
		pagina = pg;
	}
	
	var servico = $('#iniServ').val(),
	preco = $('#range_03').val(),
	ordem = $('#slOrdem').val();
	
	if (ordem == null) {
		ordem = 0;
	}

	$.ajax( {
		type: 'GET',
		url: 'BuscaServicoAsync',
		dataType: 'html',
		data: {
			servico: servico,
			preco: preco,
			ordem: ordem,
			pagina: pagina
		},
		success: function(data) {
			$('#resultados').html(data);
		} 
	});
}