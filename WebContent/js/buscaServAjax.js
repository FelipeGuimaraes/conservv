// AJAX para busca de servicos
$(document).ready(function() {
	$('#frmIni').submit(function(event) {
		event.preventDefault();
		
		var servico = $('#iniServ').val(),
			preco = $('#range_03').val(),
			ordem = $('#slOrdem').val();
			
			if (ordem == null) {
				ordem = 0;
			}
		
		$.ajax( {
			type: 'GET',
			url: 'BuscaServicoAsync',
			dataType: 'html',
			data: {
				servico: servico,
				preco: preco,
				ordem: ordem,
				pagina: 0
			},
			success: function(data) {
				$('#resultados').html(data);
			} 
		});
		
	});
});
