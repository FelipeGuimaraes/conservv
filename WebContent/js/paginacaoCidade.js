var pagina = 0;

function mudarPagina(pg) {
	if (pg == 'anterior') {
		if (pagina > 0) {
			pagina -= 1;
		}
	} else if (pg == 'proxima') {
		pagina += 1;
	} else {
		pagina = pg;
	}
	
	var cidade = $('#iniServ').val();
	
	$.ajax( {
		type: 'GET',
		url: 'BuscaCidadeAsync',
		dataType: 'html',
		data: {
			cidade: cidade,
			pagina: pagina
		},
		success: function(data) {
			$('#resultados').html(data);
		} 
	});
}