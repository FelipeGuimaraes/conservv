$(document).ready(function() {
	$('#frmIni').submit(function(event) {
		event.preventDefault();
		
		var cidade = $('#iniServ').val();
		
		$.ajax( {
			type: 'GET',
			url: 'BuscaCidadeAsync',
			dataType: 'html',
			data: {
				cidade: cidade
			},
			success: function(data) {
				$('#resultados').html(data);
			} 
		});
		
	});
});