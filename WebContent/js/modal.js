/*var modal = document.getElementById("mostrar");

var clicado = document.getElementsByClassName("clicado");

clicado[0].onclick = function () {
    alert("oi");
    clicado = $(this).val();
    $('#mostrarId').html(clicado);
}*/

function printModal() {
	var conteudo = document.getElementById('print').innerHTML;
	tela_impressao = window.open('about:blank');
	tela_impressao.document.write(conteudo);
	tela_impressao.window.print();
	tela_impressao.window.close();
}

function atualizarModalServ(id) {
	var cell = $('#resultados tr:eq(' + id + ')');

	var pj = cell.find('#item_pj').text(),
		cnpj = cell.find('#item_cnpj').text(),
		endereco = cell.find('#item_end').text(),
		cidade = cell.find('#item_cidade').text(),
		estado = cell.find('#item_estado').text(),
		cep = cell.find('#item_cep').text(),
		telefone = cell.find('#item_tel').text(),
		email = cell.find('#item_email').text(),
		seguro = cell.find('#item_seguro').text(),
		presencial = cell.find('#item_pres').text(),
		duracao = cell.find('#item_duracao').text(),
		valor = cell.find('#item_valor').text();

	// Cria o texto de informacoes de seguro e tipo de servico (presencial ou a distancia)
	var seguro_pres = '';
	if (seguro.trim() == 'true') {
		seguro_pres += 'Com seguro';
	} else {
		seguro_pres += 'Sem seguro';
	}

	seguro_pres += ' | ';

	if (presencial.trim() == 'P') {
		seguro_pres += 'Presencial';
	} else {
		seguro_pres += 'À distância';
	}

	// Insere os dados no modal
	var modal = $('#modal_dados');

	modal.find('#modal_pj').html(pj);
	modal.find('#modal_cnpj').html('<strong>CNPJ: </strong>' + cnpj);
	modal.find('#modal_end').html('<strong>Endereço: </strong>' + endereco + ', ' + cidade + '/' + estado);
	modal.find('#modal_cep').html('<strong>CEP: </strong>' + cep);
	modal.find('#modal_tel').html('<strong>Telefone: </strong>' + telefone);
	modal.find('#modal_email').html('<strong>E-mail: </strong>' + email);
	modal.find('#modal_seg_pres').html('<strong>' + seguro_pres + '</strong>');
	modal.find('#modal_duracao').html('<strong>Duração: </strong>' + duracao);
	modal.find('#modal_valor').html('<strong>Valor: </strong>' + valor);

}

function atualizarModalCidade(id) {
	var cell = $('#resultados tr:eq(' + id + ')');

	var pj = cell.find('#item_pj').text(),
		cnpj = cell.find('#item_cnpj').text(),
		endereco = cell.find('#item_end').text(),
		cidade = cell.find('#item_cidade').text(),
		estado = cell.find('#item_estado').text(),
		cep = cell.find('#item_cep').text(),
		telefone = cell.find('#item_tel').text(),
		email = cell.find('#item_email').text(),
		servicos = cell.find('#item_serv span');

	// insere dados no modal
	var modal = $('#dados');

	modal.find('#modal_pj').html(pj);
	modal.find('#modal_cnpj').html('<strong>CNPJ: </strong>' + cnpj);
	modal.find('#modal_end').html('<strong>Endereço: </strong>' + endereco + ', ' + cidade + '/' + estado);
	modal.find('#modal_cep').html('<strong>CEP: </strong>' + cep);
	modal.find('#modal_tel').html('<strong>Telefone: </strong>' + telefone);
	modal.find('#modal_email').html('<strong>E-mail: </strong>' + email);

	// limpa a div de servicos e insere os servicos
	modal.find('#modal_serv p').remove();
	servicos.each(function () {
		modal.find('#modal_serv').append('<p>' + $(this).text() + '</p>');
	});

	initMapAddress(endereco + ', ' + cidade + '/' + estado);
}

function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 15,
		center: { lat: -34.397, lng: 150.644 }
	});
	var geocoder = new google.maps.Geocoder();

	var _endereco = 'Av Dr Armando Pannunzio, 1893 - Sorocaba/SP';

	geocodeAddress(geocoder, map, _endereco);
}

function initMapAddress(x) {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 15,
		center: { lat: -34.397, lng: 150.644 }
	});
	var geocoder = new google.maps.Geocoder();

	geocodeAddress(geocoder, map, x);
}

function geocodeAddress(geocoder, resultsMap, address) {
	geocoder.geocode({ 'address': address }, function (results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: resultsMap,
				position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}
