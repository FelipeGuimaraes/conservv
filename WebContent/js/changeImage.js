var imgList = ['homem1.png', 'homem2.png', 'homem3.png', 'homem4.png', 'homem5.png', 'homem6.png', 'homem7.png', 'mulher.png'];
var counter = 0;
var delay = 4000;
var transitionTime = 500;


$(document).ready(() => {

  setInterval(() => {
    img = imgList[counter];
    counter++;

    if (counter >= imgList.length - 1) {
      counter = 0;
    }

    $('#img-page').fadeOut(transitionTime, function() {
      $(this).attr('src', 'imagens/' + img).fadeIn(transitionTime);
    })
  }, delay);
});