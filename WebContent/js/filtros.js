function filtroPresencial() {
	var flagFiltro = $('#cbPresencial').prop('checked');
	
	// Itera a tabela de resultados e verifica se o servico eh presencial
	$('#resultados tr').each(function() {
		var tipo = $(this).find('#item_pres').text();
		
		// Se flag filtro ativa, esconde os servicos q nao sao presenciais
		if (flagFiltro && tipo.trim() != 'P') {
			$(this).addClass('filtrarPres');
			
		// Se flag nao ativa, exibe os resultados escondidos
		} else if ($(this).hasClass('filtrarPres')) {
			$(this).removeClass('filtrarPres');
		}
	});
}

function filtroSeguro() {
	var flagFiltro = $('#cbSeguro').prop('checked');
	
	// Itera a tabela de resultados e verifica se o servico possui seguro
	$('#resultados tr').each(function() {
		var seguro = $(this).find('#item_seguro').text();
		
		// Se flag filtro ativa, esconde os servicos q nao possuem seguro
		if (flagFiltro && seguro.trim() != 'true') {
			$(this).addClass('filtrarSeguro');
		
		// Se flag nao ativa, exibe os resultados escondidos
		} else if ($(this).hasClass('filtrarSeguro')) {
			$(this).removeClass('filtrarSeguro');
		}
	});
}