package models;

public class Realiza {
	PessoaJuridica pessoaJuridica;
	Servico servico;
	float valor;
	boolean seguro;
	int duracao;
	
	public Realiza(PessoaJuridica pessoaJuridica, Servico servico, float valor, boolean seguro, int duracao) {;
		this.pessoaJuridica = pessoaJuridica;
		this.servico = servico;
		this.valor = valor;
		this.seguro = seguro;
		this.duracao = duracao;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public boolean isSeguro() {
		return seguro;
	}

	public void setSeguro(boolean seguro) {
		this.seguro = seguro;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

}
