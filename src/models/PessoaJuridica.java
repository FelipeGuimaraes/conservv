package models;

import java.util.*;

public class PessoaJuridica {
	private String cnpj;
	private String nome;
	private String endereco;
	private String cep;
	private String cidade;
	private String estado;
	private String telefone;
	private String email;
	ArrayList<Servico> servicoList = new ArrayList<Servico>();
	
	public PessoaJuridica() {};
	
	public PessoaJuridica(String cnpj, String nome, String endereco, String cep, String estado, String cidade, String telefone, String email) {
		this.cnpj = cnpj;
		this.nome = nome;
		this.endereco = endereco;
		this.cep = cep;
		this.estado = estado;
		this.cidade = cidade;
		this.telefone = telefone;
		this.email = email;
	}
	
	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEndereco() {
		return endereco;
	}
	
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String getCep() {
		return cep;
	}
	
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public ArrayList<Servico> getServicoList() {
		return servicoList;
	}

	public void setServicoList(ArrayList<Servico> servicoList) {
		this.servicoList = servicoList;
	}
	
	public String getEstado() {
		return this.estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
