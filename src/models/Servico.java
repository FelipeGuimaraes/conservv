package models;

import java.util.ArrayList;

public class Servico {
	String descricao;
	int codigo;
	String nomeServico;
	char tipo;
	ArrayList<PessoaJuridica> pessoaJuridicaList = new ArrayList<PessoaJuridica>();
	
	public Servico(String descricao, int codigo, String nomeServico, char tipo) {
		this.descricao = descricao;
		this.codigo = codigo;
		this.nomeServico = nomeServico;
		this.tipo = tipo;
	}
	
	// Usado na consulta 2 de pessoas juridicas (e seus servicos) por cidade
	// Já que nessa listagem, a unica informacao necessaria do servico eh o nome
	public Servico(String nomeServico) {
		this.nomeServico = nomeServico;
	}
	
	// Usado na consulta 1 por servicos
	public Servico(String nomeServico, char tipo) {
		this.nomeServico = nomeServico;
		this.tipo = tipo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String getNomeServico() {
		
		return nomeServico;
	}
	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}
	
	public char getTipo() {
		return tipo;
	}
	
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public ArrayList<PessoaJuridica> getPessoaJuridicaList() {
		return pessoaJuridicaList;
	}

	public void setPessoaJuridicaList(ArrayList<PessoaJuridica> pessoaJuridicaList) {
		this.pessoaJuridicaList = pessoaJuridicaList;
	}
}
