package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dados.DAOException;
import dados.PessoaJuridicaDAO;
import models.PessoaJuridica;

@WebServlet("/BuscaCidade")
public class BuscaCidade extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// Recupera a lista de pessoas juridicas que atuam na cidade passada na requisicao
			PessoaJuridicaDAO pjdao = new PessoaJuridicaDAO();
			String cidade = request.getParameter("cidade");
			ArrayList<PessoaJuridica> pjs = pjdao.getPessoaJuridicaByCity(cidade);
			
			// Adiciona a lista de pessoas juridicas recuperadas na requisicao do usuario
			request.setAttribute("pessoas_juridicas", pjs);
			
			
		} catch (DAOException e) {
			e.printStackTrace();
		}
		
		// Redireciona o usuario para a pagina de resultados com os resultados obtidos
		RequestDispatcher dispatcher = request.getRequestDispatcher("/pjs.jsp");
		dispatcher.forward(request, response);
	}

}
