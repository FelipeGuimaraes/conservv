package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.MaskFormatter;

import dados.DAOException;
import dados.PessoaJuridicaDAO;
import models.PessoaJuridica;
import models.Servico;

@WebServlet("/BuscaCidadeAsync")
public class BuscaCidadeAsync extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// Recupera a lista de pessoas juridicas que atuam na cidade passada na requisicao
			PessoaJuridicaDAO pjdao = new PessoaJuridicaDAO();
			String cidade = request.getParameter("cidade");
			int pagina = Integer.parseInt(request.getParameter("pagina"));
			
			ArrayList<PessoaJuridica> pjs = pjdao.getPessoaJuridicaByCity(cidade);
			
			// Adiciona os resultados na requisicao do usuario
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			String result = "";
			
			// Inicializa masks
			MaskFormatter mask_cnpj = new MaskFormatter("##.###.###/####-##");
            MaskFormatter mask_tel = new MaskFormatter("(##) ####-####");
            MaskFormatter mask_cep = new MaskFormatter("##.###-###");

            mask_cnpj.setValueContainsLiteralCharacters(false);
            mask_tel.setValueContainsLiteralCharacters(false);
            mask_cep.setValueContainsLiteralCharacters(false);
			
            int id = 0; // id do modal
            
            if (pjs.size() < 0) {
            	result = "<tr id=\"nenhumRegistro\">Nenhum registro encontrado!</tr>";
            } else {
            	for (int i = pagina*10; i < (pagina + 1)*10; i++) {
            		result += "<tr>";
            		result += "<td id=\"item_pj\">" + pjs.get(i).getNome() + " </td>";
            		result += "<td id=\"item_cidade\">" + pjs.get(i).getCidade() + "</td>";
            		result += "<td id=\"item_tel\">" + mask_tel.valueToString(pjs.get(i).getTelefone()) + "</td>";
            		result += "<td> <a class=\"btn btn-primary\" href=\"#dados\" rel=\"modal:open\" onclick=\"atualizarModalCidade(" + id + ")\">+</a> </td>";
            		result += "<td id=\"item_cnpj\" class=\"invisible\">" + mask_cnpj.valueToString(pjs.get(i).getCnpj()) + "</td>";
            		result += "<td id=\"item_end\" class=\"invisible\">" + pjs.get(i).getEndereco() + "</td>";
            		result += "<td id=\"item_estado\" class=\"invisible\">" + pjs.get(i).getEstado() + "</td>";
            		result += "<td id=\"item_cep\" class=\"invisible\">" + mask_cep.valueToString(pjs.get(i).getCnpj()) + "</td>";
            		result += "<td id=\"item_email\" class=\"invisible\">" + pjs.get(i).getEmail() + "</td>";
            		result +="<td id=\"item_serv\" class=\"invisible\">";
            		
            		// adiciona os servicos
            		for (Servico serv: pjs.get(i).getServicoList()) {
            			result += "<span>" + serv.getNomeServico() + "</span>";
            		}
            		
            		result +="</td>";
            		result += "</tr>";
            		
            		id++;
            	}
            }
			
			
			
			PrintWriter out = response.getWriter();
			out.print(result);
			out.close();
			
		} catch (DAOException e) {
			e.printStackTrace();
			e.getMessage();
		} catch (ParseException e) {
			e.printStackTrace();
			e.getMessage();
		}

	}
		
}
