package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dados.DAOException;
import dados.RealizaDAO;
import models.Realiza;

@WebServlet("/BuscaServico")
public class BuscaServico extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// Recupera a lista de empresas que prestam o servico procurado pelo usuario
			RealizaDAO realizaDao = new RealizaDAO(); 
			String servico = request.getParameter("servico");
			
			// Divide o parametro preco (divido por ';') em preco minimo e preco maximo
			String[] splitPreco = request.getParameter("preco").split(";");
 			float precoMin = Float.parseFloat(splitPreco[0]);
			float precoMax = Float.parseFloat(splitPreco[1]);
			
			ArrayList<Realiza> servicosList = realizaDao.getServicoByNameAndPrice(servico, precoMin, precoMax);
			
			// Adiciona os resultados na requisicao do usuario
			request.setAttribute("servicos", servicosList);
			
			
		} catch (DAOException e) {
			e.printStackTrace();
			e.getMessage();
		}
		
		// Redireciona o usuario para a pagina de resultados
		RequestDispatcher dispatcher = request.getRequestDispatcher("/servico.jsp");
		dispatcher.forward(request, response);
	}

}
