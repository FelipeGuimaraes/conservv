package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.MaskFormatter;

import dados.DAOException;
import dados.RealizaDAO;
import models.Realiza;

class SortAsc implements Comparator<Realiza> {
	public int compare(Realiza a, Realiza b) {
		if (a.getValor() > b.getValor()) {
			return 1;
		} else if (a.getValor() < b.getValor()) {
			return -1;
		} else {
			return 0;
		}
	}
}

class SortDesc implements Comparator<Realiza> {
	public int compare(Realiza a, Realiza b) {
		if (a.getValor() < b.getValor()) {
			return 1;
		} else if (a.getValor() > b.getValor()) {
			return -1;
		} else {
			return 0;
		}
	}
}


@WebServlet("/BuscaServicoAsync")
public class BuscaServicoAsync extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// Recupera a lista de empresas que prestam o servico procurado pelo usuario
			// Com os filtros desejados
			RealizaDAO realizaDao = new RealizaDAO(); 
			String servico = request.getParameter("servico");
			int ordem = Integer.parseInt(request.getParameter("ordem"));
			int pagina = Integer.parseInt(request.getParameter("pagina"));
			
			
			// Divide o parametro preco (divido por ';') em preco minimo e preco maximo
			String[] splitPreco = request.getParameter("preco").split(";");
 			float precoMin = Float.parseFloat(splitPreco[0]);
			float precoMax = Float.parseFloat(splitPreco[1]);
			
			ArrayList<Realiza> servicosList = realizaDao.getServicoByNameAndPrice(servico, precoMin, precoMax);
			
			if (ordem == 1) {
				Collections.sort(servicosList, new SortAsc());
			} else if (ordem == 2) {
				Collections.sort(servicosList, new SortDesc());
			}
			
			// Adiciona os resultados na requisicao do usuario
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			String result = "";
			
			// Inicializa masks
			MaskFormatter mask_cnpj = new MaskFormatter("##.###.###/####-##");
            MaskFormatter mask_tel = new MaskFormatter("(##) ####-####");
            MaskFormatter mask_cep = new MaskFormatter("##.###-###");

            mask_cnpj.setValueContainsLiteralCharacters(false);
            mask_tel.setValueContainsLiteralCharacters(false);
            mask_cep.setValueContainsLiteralCharacters(false);
			
            int id = 0; // id do modal
            
            // Constroi o HTML
            if (servicosList.size() < 0) {
            	result = "<tr id=\"nenhumRegistro\">Nenhum registro encontrado!</tr>";
            } else {
            	for (int i = pagina*10; i < (pagina + 1)*10; i++) {
            		
            		result += "<tr>";
            		result += "<td> " + servicosList.get(i).getServico().getNomeServico() + " </td>";
            		result += "<td id=\"item_pj\">" + servicosList.get(i).getPessoaJuridica().getNome() + " </td>";
            		result += "<td id=\"item_duracao\">" + servicosList.get(i).getDuracao() + " meses</td>";
            		result += "<td id=\"item_valor\">R$ " + servicosList.get(i).getValor() + " </td>";
            		result += "<td> <a class=\"btn btn-primary\" href=\"#modal_dados\" rel=\"modal:open\" onclick=\"atualizarModalServ(" + id + ")\">+</a> </td>";
            		result += "<td id=\"item_cnpj\" class=\"invisible\">" + mask_cnpj.valueToString(servicosList.get(i).getPessoaJuridica().getCnpj()) + "</td>";
            		result += "<td id=\"item_end\" class=\"invisible\">" + servicosList.get(i).getPessoaJuridica().getEndereco() + "</td>";
            		result += "<td id=\"item_cep\" class=\"invisible\">" + mask_cep.valueToString(servicosList.get(i).getPessoaJuridica().getCep()) + "</td>";
            		result += "<td id=\"item_tel\" class=\"invisible\">" + mask_tel.valueToString(servicosList.get(i).getPessoaJuridica().getTelefone()) + "</td>";
            		result += "<td id=\"item_email\" class=\"invisible\">" + servicosList.get(i).getPessoaJuridica().getEmail() + "</td>";
            		result += "<td id=\"item_seguro\" class=\"invisible\">" + servicosList.get(i).isSeguro() + "</td>";
            		result += "<td id=\"item_pres\" class=\"invisible\">" + servicosList.get(i).getServico().getTipo() + "</td>";
            		result += "<td id=\"item_cidade\" class=\"invisible\">" + servicosList.get(i).getPessoaJuridica().getCidade() + "</td>";
            		result += "<td id=\"item_estado\" class=\"invisible\">" + servicosList.get(i).getPessoaJuridica().getEstado() + "</td>";
            		result += "</tr>";
            		
            		id++;
            	}
            }
			
			PrintWriter out = response.getWriter();
			out.print(result);
			out.close();
			
		} catch (DAOException e) {
			e.printStackTrace();
			e.getMessage();
		} catch (ParseException e) {
			e.printStackTrace();
			e.getMessage();
		}
	}

}
