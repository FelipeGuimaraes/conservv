package dados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.PessoaJuridica;
import models.Servico;

public class PessoaJuridicaDAO {
	private Connection con;
	
	public PessoaJuridicaDAO() throws DAOException {
		this.con = ConnectionFactory.getConnection();
	}
	
	public ArrayList<PessoaJuridica> getPessoaJuridicaByCity(String cidadeUsuario) throws DAOException {
		ArrayList<PessoaJuridica> pessoaJuridicaList = new ArrayList<PessoaJuridica>();
		String pjAnterior;
		
		// Dados retornados da consulta
		String nome = "null", endereco, estado, cidade, email, nomeServico, cnpj, cep, telefone;
		
		try {
			String query = "SELECT * FROM ConsultaCidade(?)";			
			
			PreparedStatement statement = con.prepareStatement(query);
			statement.setString(1, cidadeUsuario);
			ResultSet rs = statement.executeQuery();
			
			//if (notEmpty) {
			//}
						
			while (rs.next()) {
				pjAnterior = nome;
				
				nome = rs.getString("nome_t");
				cnpj = rs.getString("cnpj_t");
				endereco = rs.getString("endereco_t");
				cep = rs.getString("cep_t");
				estado = rs.getString("estado_t");
				cidade = rs.getString("cidade_t");
				telefone = rs.getString("telefone_t");
				email = rs.getString("email_t");
				nomeServico = rs.getString("nome_servico");
				
				if (!pjAnterior.equals(nome)) { // Se empresa nao existe no array, insere no array de empresas
					PessoaJuridica pj = new PessoaJuridica(cnpj, nome, endereco, cep, estado, cidade, telefone, email);
					pj.setServicoList(new ArrayList<Servico>());
					pessoaJuridicaList.add(pj);
				}
				
				Servico serv = new Servico(nomeServico);
				pessoaJuridicaList.get(pessoaJuridicaList.size() - 1).getServicoList().add(serv);
				
			} // while
			statement.close();
			return pessoaJuridicaList;
		} catch (SQLException e) {
			e.printStackTrace();
			e.getMessage();
			throw new DAOException();
		}
	}
}
