package dados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.PessoaJuridica;
import models.Realiza;
import models.Servico;

public class RealizaDAO {
	private Connection con;
	
	public RealizaDAO() throws DAOException {
		this.con = ConnectionFactory.getConnection();
	}
	
	public ArrayList<Realiza> getServicoByNameAndPrice(String servico, float precoMin, float precoMax) 
			throws DAOException {
		ArrayList<Realiza> servicosList = new ArrayList<Realiza>();
		
		// Dados retornados da consulta
		String cnpj, nome, endereco, cep, estado, cidade, telefone, email;
		char tipo;
		int duracao;
		float valor;
		boolean seguro;
		
		try {
			String query = "SELECT * FROM ConsultaServico(?, ?, ?)";
			
			PreparedStatement statement = con.prepareStatement(query);
			statement.setString(1, servico);
			statement.setFloat(2, precoMin);
			statement.setFloat(3, precoMax);
			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				cnpj = rs.getString("cnpj_t");
				nome = rs.getString("nome_t");
				endereco = rs.getString("endereco_t");
				cep = rs.getString("cep_t");
				estado = rs.getString("estado_t");
				cidade = rs.getString("cidade_t");
				telefone = rs.getString("telefone_t");
				email = rs.getString("email_t");
				tipo = rs.getString("tipo_t").charAt(0);
				duracao = rs.getInt("duracao_t");
				valor= rs.getFloat("valor_t");
				seguro = rs.getBoolean("seguro_t");
				
				PessoaJuridica pj = new PessoaJuridica(cnpj, nome, endereco, cep, estado, cidade, telefone, email);
				Servico serv = new Servico(servico, tipo);
				
				Realiza realizaServ = new Realiza(pj, serv, valor, seguro, duracao);
				
				servicosList.add(realizaServ);
			}
			
			return servicosList;
			
		} catch (SQLException e) {
			e.printStackTrace();
			e.getMessage();
			throw new DAOException();
		}
		
	}

}
